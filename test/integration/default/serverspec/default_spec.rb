require 'serverspec'

set :backend, :exec

describe docker_image('memcached') do
  it { should exist }
end

describe docker_container('ecmc-memcache') do
  its(:HostConfig_NetworkMode) { should eq 'docker_net'}
  its(:Args) { should include('memcached', '-m', '64') }
  it { should be_running }
end
