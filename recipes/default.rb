#
# Cookbook Name:: ecmc_memcached
# Recipe:: default
#
# Copyright 2016, University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
docker_image 'memcached'

docker_container 'ecmc-memcache' do
  repo 'memcached'
  network_mode 'docker_net'
  command 'memcached -m 64'
  restart_policy 'always'
end
