name             'ecmc_memcached'
maintainer       'University of Edinburgh'
maintainer_email 'paul.d.mitchell@ed.ac.uk'
license          'All rights reserved'
description      'Installs/Configures Memcached'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
